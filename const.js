const router = {
  THEM_MOI_QUY_TRINH: "https://bpm-real.vercel.app/them-moi-quy-trinh.html",
  CHI_TIET_QUY_TRINH: "https://bpm-real.vercel.app/chi-tiet-quy-trinh.html",
  DUNG_QUY_TRINH: "https://bpm-real.vercel.app/dung-quy-trinh.html",
  CHI_TIET_WORKFLOW: "https://bpm-real.vercel.app/chi-tiet-workflow.html",
  CAP_NHAT_PHIEN_BAN_WORKFLOW:
    "https://bpm-real.vercel.app/cap-nhat-phien-ban-chi-tiet-workflow.html",
  DUNG_WORKFLOW: "https://bpm-real.vercel.app/dung-workflow.html",
  LICH_SU_THAY_DOI: "https://bpm-real.vercel.app/lich-su-thay-doi.html",
  LICH_SU_VERSION: "https://bpm-real.vercel.app/lich-su-version.html",
};
