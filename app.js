document.addEventListener("DOMContentLoaded", function () {
  app.start();
});

const app = {
  initlib: function () {
    // $("#exampleModal").modal("show");

    $(document).on("focus", ".datepicker-custom", function () {
      console.log("////////////////////");
      $(this).datetimepicker({
        format: "DD/MM/YYYY",
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down",
          previous: "fa fa-chevron-left",
          next: "fa fa-angle-right",
          today: "fa fa-clock-o",
          clear: "fa fa-trash-o",
        },
      });
    });
    $('input[name="daterange"]').daterangepicker(
      {
        opens: "left",
      },
      function (start, end, label) {
        console.log(
          "A new date selection was made: " +
            start.format("YYYY-MM-DD") +
            " to " +
            end.format("YYYY-MM-DD")
        );
      }
    );
  },
  start: function () {
    this.initlib();
  },
};
